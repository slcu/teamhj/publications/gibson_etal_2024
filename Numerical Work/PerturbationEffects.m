%Function calculating eigenvalues of order parameter for each spherical
%harmonic perturbation (with lower index n), multiplied by a factor of p.
function T=bif(n,p)



%Set Legendre coefficient index we want to focus on
T=zeros(3,2*n+1);
for i=-n:n
   
%Define functions we want to integrate.
fun=@(x,y) sin(x);
fun11=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*cos(y).^2.* sin(x).^2-1/2);
fun12=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*sin(y).*cos(y).*sin(x).^2);
fun13=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*cos(y).*sin(x).*cos(x));
fun22=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*sin(y).^2.*sin(x).^2-1/2);
fun23=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*sin(y).*sin(x).*cos(x));
fun33=@(x,y) sin(x).*(1+p*harmonicY(n,i,x,y,'type','real')).*(3/2*cos(x).^2-1/2);

%Define normalising denominator of order parameter.
norm=integral2(fun, 0, pi, 0, 2*pi);

%Perform integrals to obtain our order parameter, a 3x3 matrix we denote by
%S.
S=zeros(3,3);
S(1,1)=integral2(fun11, 0, pi,0,2*pi)/norm;
S(1,2)=integral2(fun12, 0, pi,0,2*pi)/norm;
S(1,3)=integral2(fun13, 0, pi,0,2*pi)/norm;
S(2,2)=integral2(fun22, 0, pi,0,2*pi)/norm;
S(2,3)=integral2(fun23, 0, pi,0,2*pi)/norm;
S(3,3)=integral2(fun33, 0, pi,0,2*pi)/norm;

%Exploit symmetry of order parameter (note that it is already traceless by
%definition).
S(2,1)=S(1,2);
S(3,2)=S(2,3);
S(3,1)=S(1,3);
T(1:3,i+n+1)=eig(S);
end





