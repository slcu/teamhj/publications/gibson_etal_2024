% Calculated plots for Fig. 5 in Gison et. al. 2023 . Designed so it will run in Octave so
%rather than calculating the appropraite coefficients these are input directly as vectors and then
% plotted. To calculate the coefficients use the Matalb script LegendreCoefficients.m

%Coefficient values copied across
z=[2.1930,1.2661, 0.9807, 0.8289, 0.7310, 0.6612,0.6082,0.5662, ...
    0.5319,0.5031,0.4785,0.4573,0.4386,0.4220,0.4072,0.3939,0.3817, ...
    0.3707,0.3605,0.3512];

q=zeros(3,22);
q(1,:)=0:21;
q(2,:)=[0.4334,0,-0.5053,0,0.1051,0,-0.0534, 0,0.0342, 0,-0.0244, ...
         0, 0.0186,0,-0.0147,0,0.0121, 0, -0.0102,0, 0.0087,0];
q(3,:)=[ 0, NaN, 1.5616, NaN, 12.9999,NaN, 24.2704, NaN, 40.2209, NaN, ...
   57.1648, NaN, 77.7127,NaN,99.4164,NaN,124.1868,NaN,150.1511, ...
       NaN, 178.8428,NaN];



hold off
figure(1)
set(gca,'fontsize',20)
red=plot(1:20,z(1:20),'Color','r','LineWidth',2);
hold on
ylim([-0.6 0.6])
yticks([-0.6 -0.4 -0.2 0 0.2 0.4 0.6])
blue=plot(q(1,1:2:21),q(2,1:2:21),'b','LineWidth',2);
plot(1:20,-z(1:20),'r','LineWidth',2)
xlim([0 20])
xticks([0 10 20])
set(gca,'fontsize',20)
leg=legend([red,blue],{'Bounds','Legendre Coefficients'});
plot([-1,21],[0.351,0.351],'k','LineWidth',2)
plot([-1,21],[-0.351,-0.351],'k','LineWidth',2)
xlim([0,20])

% Draw graph of $G^*_\ell$ for Legendre coefficients $l=2,...,20$, which
% are values of the control parameter for which a phase transition can
% occur.
  figure(2)
  plot(q(1,3:2:21),q(3,3:2:21),'-ok','LineWidth',2)
  xlim([0 20])
  xticks([0 10 20])
  ylim([0 200])
  yticks([0 50 100 150 200])
  set(gca,'fontsize',20)