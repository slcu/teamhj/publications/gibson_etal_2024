%
% Definition of functions associated to induced catastrophe and zippering 
% flux terms,with Legendre polynomials included for following Legendre
% coefficient calculations.

q=zeros(3,100);
fun_1=@(x,l) sin(x).^2.*(x./2).*legendreP(l,cos(x));
fun_z_1=@(x) sin(x).^2.^(x./2).*legendreP(0,cos(x));
fun_z_2=@(x) sin(x).^2.^(pi-x./2).*legendreP(0,cos(x));
z_0=1/2*(integral(@(x) fun_z_1(x),0,0.7)+integral(@(x) fun_z_2(x),pi-0.7,pi));
% 
% Calculation of Legendre coefficients associated to the 
% probability of induced catastrophe function q(2,j) for
% each Legendre index q(1,j). The corresponding control parameter value for
% which a phase transition could occur is also calculated as q(3,j).

for j=1:22
n=j-1;
q(1,j)=n;
q(2,j)=(n+1/2)*(1+(-1)^n)*(integral(@(x) fun_1(x,n),0,pi/2));
if j>1
q(3,j)=-nthroot(-8*pi*q(2,j)/(2*n+1),3)*(1+(n+1/2)*q(2,1)/q(2,j)); 
end
end
% 
% Numerical calculation of upper and lower bounds of Legendre coefficients
% corresponding to induced catastophe flux term, using improved Berenstein
% inequality.

syms x_1 x_2
y_11=abs(sin(acos(x_1))).*(pi/2-acos(x_1)/2);
y_21=abs(sin(acos(x_2))).*(acos(x_2)/2);
y_12=diff(y_11,x_1);
y_22=diff(y_21,x_2);
y_1=abs(y_12)*(1-x_1^2)^(-1/4);
y_2=abs(y_22)*(1-x_2^2)^(-1/4);
z=zeros(100,1);
p=vpa(int(y_1,x_1,[-1 0]))+vpa(int(y_2,x_2,[0 1]));
for n=1:20
z(n)=2*(pi*(2*n-1))^(-1/2)*p;
end
% 
% Draw graph of $C_l$ for $l=2,...,20$. Bounds from
% improved Berenstein inequality are also graphed.

hold off
set(gca,'fontsize',20)
red=plot(1:20,z(1:20),'Color','r');
hold on
ylim([-0.6 0.6])
yticks([-0.6 -0.4 -0.2 0 0.2 0.4 0.6])
blue=plot(q(1,1:2:21),q(2,1:2:21),'b');
plot(1:20,-z(1:20),'r')
xlim([0 20])
xticks([0 10 20])
set(gca,'fontsize',20)
leg=legend([red,blue],'Bounds','Legendre Coefficients');
yline(.351,'green','LineWidth',2)
yline(-.351,'green','Linewidth',2)
leg.String(end)=[];
leg.String(end)=[];

% Draw graph of $G^*_\ell$ for Legendre coefficients $l=2,...,20$, which
% are values of the control parameter for which a phase transition can
% occur.
  figure(2)
  plot(q(1,3:2:21),q(3,3:2:21),'-ok')
  xlim([0 20])
  xticks([0 10 20])
  ylim([0 200])
  yticks([0 50 100 150 200])
  set(gca,'fontsize',20)
