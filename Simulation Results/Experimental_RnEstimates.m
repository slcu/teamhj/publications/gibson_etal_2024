%Calculate the rn estimates in SecIIIE Table 1
coeff=@(x) -((4*pi*x(1)*x(2)/(x(1)+x(2)))^(1/3))*((0.024)^(-1/3))*(x(3)/x(2)-x(4)/x(1));
rn_est=@(x) (-((4*pi*x(1)*x(2)/(x(1)+x(2)))^(1/3))*((0.024)^(-1/3))*(x(3)/x(2)-x(4)/x(1))/1.56)^3;

x=[4.59,18.36,0.051*60,0.015*60]; %interphase MTs
x1=[6.88,17.89,0.065*60,0.029*60]; %preprophase MTs
x2=[1.9,9.7,0.005*60,0.002*60]; %suspension MTs

disp("Interphase MTs:"); disp(rn_est(x))
disp("Rreprophase MTs:"); disp(rn_est(x1))
disp("Suspension MTs:"); disp(rn_est(x2))