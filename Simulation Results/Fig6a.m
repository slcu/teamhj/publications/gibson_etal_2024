%Fig. 6 in Gibson et. al. 
%Plotting a path in the (r_n,r_c) plane which gives constant microtubule
%density.
hold off
r_c_1=[0.00025, 0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035];
r_n_1=[0.067,0.11, 0.24, 0.43, 0.65, 0.95, 1.25, 1.6];
r_n_2=[0.0245, 0.0425, 0.086, 0.16, 0.245, 0.35, 0.47, 0.58];
%r_n_1=[0.044 0.15 0.32 0.55 0.9 1.25 1.8];
%r_n_2=[0.088 0.3 0.64 1.1 1.8 2.5 3.6];
RLen=length(r_c_1);
for i=1:RLen
    r_n_1(i)=r_n_1(i)/(4/3*pi*700^3);
    r_n_2(i)=r_n_2(i)/(4/3*pi*700^3);
end
plot(log10(r_c_1),log10(r_n_1),'-o','Color',[0.3010 0.7450 0.9330],'LineWidth',2)
hold on
plot(log10(r_c_1),log10(r_n_2),'-o','Color',[0.8500 0.3250 0.0980],'LineWidth',2)
set(gca,'fontsize',20)
xlabel('log_{10}(r_c)','fontsize',24)
ylabel('log_{10}(r_n)','fontsize',24)

c=polyfit(log10(r_c_1(3:end)),log10(r_n_1(3:end)),1);
y1=polyval(c,log10(r_c_1));
f1=['y=',num2str(c(1)),'x',num2str(c(2))];
plot(log10(r_c_1),y1,'--','Color',[0.3010 0.7450 0.9330],'LineWidth',2)

c=polyfit(log10(r_c_1(3:end)),log10(r_n_2(3:end)),1);
y1=polyval(c,log10(r_c_1));
plot(log10(r_c_1),y1,'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2)
f2=['y=',num2str(c(1)),'x',num2str(c(2))];

%xlim([0 0.004])
legend("42,000 segments","16,000 segments",f1,f2,'Location','northwest')

box on
set(gcf,'color','w');
ylim([-11,-8.8])