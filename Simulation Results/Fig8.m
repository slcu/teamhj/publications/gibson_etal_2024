%Plotting Anisotropy vs G_eff with zippering as before, with zippering and
%severing, and with zippering and severing with r_n adjusted to give
%constant density.
r_c=[0.00025 0.0005 0.001 0.0015 0.002 0.0025 0.003 0.0035];
r_n=[0.0245 0.042 0.086 0.16 0.245 0.35 0.47 0.58];
r_n_sevSameZip=[0.0245, 0.042, 0.086, 0.16, 0.245, 0.35, 0.47, 0.58];
r_n_sevconstant= [0.078, 0.092, 0.165, 0.25, 0.34, 0.44, 0.58, 0.76];
%Anisotropy for zippering (Aniso_zip)*
%Anisotropy for varying density (Aniso_sevSameZip)
%Anisotropy for constant density (Aniso_sevconstant)
Aniso_zip=[0.16691029733228985, 0.15957420372289108, 0.14964259247676395, 0.1401401841472922, 0.12816104518119711, 0.12882695949311543, 0.11396061529258276, 0.1298750273828529];
Aniso_sevSameZip=[0.24238287355333504, 0.20761169267435317, 0.19056064809944695, 0.17460209806216956, 0.15066854711482375, 0.13830320911092614, 0.14354979299066684, 0.13824569416426472];
Aniso_sevconstant=[0.15810074052829182, 0.13759941954563065, 0.14313926994487033, 0.12861046277297794, 0.13114307715478535, 0.12945603265852776, 0.1226522621708978, 0.12072605064063315];

%% Density with no-severing with just zippering
%Calculate G_eff for rn
LenRn=length(r_n);
G_eff=zeros(1,LenRn);
l_0=zeros(1,LenRn);
for i=1:LenRn
r_n(i)=r_n(i)/(4/3*pi*700^3);
v(i)=1;
end
for j=1:LenRn
l_0(j)=(2*pi.*v(j)./r_n(j))^(1/4);
d_m=49/8;
g(j)=(-r_c(j)./v(j));
G(j)=g(j)*l_0(j);
G_eff(j)=-G(j)*(d_m/l_0(j))^(-1/3);
end

hold off
plot(G_eff,Aniso_zip,':o','LineWidth',2,'Color','k')
hold on
xlim([0 5])
ylim([0.1 0.25])
yticks([0 0.05 0.1 0.15 0.2 0.25])
xticks([0 1 2 3 4 5])
ylabel('S','fontsize',25,'interpreter','latex')
xlabel('$G_{\mathrm{eff}}$','fontsize',25,'interpreter','latex')
set(gca,'fontsize',25)

%% Density varying with rn same as zippering case
LenRn=length(r_n_sevSameZip);
v=zeros(1,LenRn);
g=zeros(1,LenRn);
G=zeros(1,LenRn);
G_eff=zeros(1,LenRn);
l_0=zeros(1,LenRn);
for i=1:LenRn
r_n_sevSameZip(i)=r_n_sevSameZip(i)/(4/3*pi*700^3);
v(i)=1;
end
for j=1:LenRn
l_0(j)=(2*pi.*v(j)./r_n_sevSameZip(j))^(1/4);
d_m=49/8;
g(j)=(-r_c(j)./v(j));
G(j)=g(j)*l_0(j);
G_eff(j)=-G(j)*(d_m/l_0(j))^(-1/3);
end
plot(G_eff,Aniso_sevSameZip,'--o','LineWidth',2,'Color','b');


%r_n_sevconstant=[0.058 0.07 0.078 0.092 0.165 0.25 0.34 0.44 0.58 0.76];
LenRn=length(r_n_sevconstant);
v=zeros(1,LenRn);
g=zeros(1,LenRn);
G=zeros(1,LenRn);
G_eff=zeros(1,LenRn);
l_0=zeros(1,LenRn);
for i=1:LenRn
r_n(i)=r_n_sevconstant(i)/(4/3*pi*700^3);
v(i)=1;
end
for j=1:LenRn
l_0(j)=(2*pi.*v(j)./r_n(j))^(1/4);
d_m=49/8;
g(j)=(-r_c(j)./v(j));
G(j)=g(j)*l_0(j);
G_eff(j)=-G(j)*(d_m/l_0(j))^(-1/3);
end

set(gcf,'color','w');
box on

plot(G_eff,Aniso_sevconstant,'-o','LineWidth',2,'Color','r')
set(gca,'fontsize',25)
legend('Without Severing','Severing- Different Density','Severing- Same Density')