%Plotting anisotropy vs G_eff with and without the presence of zippering.
hold on;

%% Add these two lists manually. They are produced by Python script DensityPlot.py
Aniso_nozip=[0.14854626759778358, 0.1436737553200956, 0.14983766900700493, 0.13804440792733166, 0.14575408158955727, 0.12736862983155567, 0.12389403985544443, 0.12708325584020874];
Aniso_zip=[0.16691029733228985, 0.15957420372289108, 0.14964259247676395, 0.1401401841472922, 0.12816104518119711, 0.12882695949311543, 0.11396061529258276, 0.1298750273828529];

%% These are rc and rn values. Ensure they correpond to the anisotropy values above
r_c=[0.00025 0.0005 0.001 0.0015 0.002 0.0025 0.003 0.0035];
%r_n=[0.002 0.008 0.02 0.086 0.16 0.245 0.35 0.47 0.58];
r_n=[0.0245 0.0425 0.086 0.16 0.245 0.35 0.47 0.58];

%% Plotting code
LenRn=length(r_n);
v=zeros(1,LenRn);
g=zeros(1,LenRn);
G=zeros(1,LenRn);
G_eff=zeros(1,LenRn);
l_0=zeros(1,LenRn);
for i=1:LenRn
r_n(i)=r_n(i)/(4/3*pi*700^3);
v(i)=1;
end
for j=1:LenRn
l_0(j)=(2*pi.*v(j)./r_n(j))^(1/4);
d_m=49/8;
g(j)=(-r_c(j)./v(j));
G(j)=g(j)*l_0(j);
G_eff(j)=-G(j)*(d_m/l_0(j))^(-1/3);
end

plot(G_eff,Aniso_nozip,'-o','LineWidth',2,'Color','r')
plot(G_eff,Aniso_zip,'-x','LineWidth',2,'Color','b')
set(gca,'fontsize',20)
%xlim([0 5])
%ylim([.06 .12])
%yticks([0.06 0.08 0.10 0.12])
%xticks([0 1 2 3 4 5])
ylabel('S','fontsize',24,'interpreter','latex')
xlabel('$G_{\mathrm{eff}}$','fontsize',24,'interpreter','latex')
xline(1.56,'green','LineWidth',2)
legend('Without Zippering','With Zippering')
set(gcf,'color','w');
box on
G_eff