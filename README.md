## Summary

This repository provides the computational material that forms the basis of the manuscript

Mean-field theory approach to three-dimensional nematic phase transitions in microtubules<br>
Cameron Gibson, Henrik Jönsson, Tamsin Spelman<br>
https://arxiv.org/abs/2112.06855.


## Tubulaton

To run microtubule simulations using Tubulaton, go to \tubulaton link. Then, download tubulaton following the installation instructions.

Instructions to run simulations and graphical outputs, go to \tubulaton\script\GibsonEtAl\readme.txt. Follow those instructions on how to use ./Run to run simulations and analysis.

## Mathematical calculation and plotting (Matlab and Octave)

All mean field numerical calculations and many of the plots are contained in this repository under the folder Numerical Calculations. Note that these are all run in MATLAB and have been tested in Matlab R2022b,

Matlab is paid for software but many of these scripts also run in the free equivlant software Octave. All scripts in the "Simulation Results" will run in Octave (although the graph x labels may not format correctly). The LegendreCoefficients.m code (to generate Fig 5) does not run in Octave (as of May 2023) as it relies on the LegendreP function coded into Matlab but not Octave. However instead you can run LegendreCoefficients_Octave.m which will plot Fig. 5 in Octave using the input coefficient values (but the coefficients are not direclty calculated in this code).

## Figures

Below is listed how each figure is generated. All .m scripts are Matlab scripts. Tubulaton scripts for generating data are found in the tubulaton
gitlab here (1) https://gitlab.com/slcu/teamHJ/tubulaton/ , specifically in the script/GibsonEtAl folder https://gitlab.com/slcu/teamHJ/tubulaton/-/tree/master/script/GibsonEtAl.

Fig. 1 Illustration manually drawn

Fig. 2 Illustartion manually drawn

Fig. 3 Output from a tubulaton script visualised in paraview. Microtubule output produced with ini file found in (1)/init/2023_GibsonEtAl\Fig3_a.ini and (1)/init/2023_GibsonEtAl\Fig3_b.ini. See main tubulaton page on how to view tubualton output in paraview. The sphere are each vtk spheres made partially transparent placed on top. vtk files for the spheres can be found in "Simulation Results/Fig3_Spheres" along with a screenshot from paraview and some additional instructions on visualisation.

Fig. 4. Graph illustration produced initially in Matlab with graphical edits added in Inkscape.

Fig. 5a. and 5b Graph illustrating mathematical equations plotted with matlab script Numerical Work/LegendreCoefficients.m . Note axis labels must be added seperately. 


Fig. 6a. Graph illustrating calculated r\_n - r\_c values plotted with matlab script Simulation Results/Fig6a.m . Values for rc and rn are hard coded on line 5-7.

Fig. 6b. Density plot data is generated with Tubulaton by running script (1)/script/GibsonEtAl/DensityData which generates the data for both densities. This outputs text files into the folder (1)/Output/PythonGeneratedOutput/AnisotropyTextFiles/ with one file for each (rn,rc) pair. The graph is plotted with the script (1)/script/GibsonEtAl/Graphing/DensityPlot.py . For this script, the file names of the output text files need to be manually written into the function Inputs() on line 14 of DensityPlot.py. 

Fig. 7 Data is obtained as output from Tubulaton by running scripts (1)/script/GibsonEtAl/Plotting Scripts/Figure 7 (with zippering) and (1)/script/GibsonEtAl/Plotting Scripts/Figure 7 (without zippering) respectively for the with and without zippering data. (Note the without zippering data can instead be taken from that generated for Fig 6b at the lower density). This generates one file for each (rn,rc) pair in (1)/Output/PythonGeneratedOutput/AnisotropyTextFiles/ . The file names of the output text files need to be manually written into the function Inputs() on line 14 of DensityPlot_Geff.py, and running this script will output the graph for Fig A1d and also on the command line two lists after "Anisotropy". Copy thes lists into the Matlab script "Simulation Results/Fig7.m" on lines 5-6. Check the rc and rn values correspond to the appropriate entries by cross-checking lines 9 and 11 with the rc and rn values output to the command line. Running the Matlab script will generate Fig.7a.

Fig. 8 Data is obtained from Tubulaton using scripts (1)/script/GibsonEtAl/Plotting Scripts/Figure 8 (with severing- different density) and (1)/script/GibsonEtAl/Plotting Scripts/Figure 8 (with severing- same density) and using the data optained for with zippering for Fig.7. This generates one file for each (rn,rc) pair for each condition in (1)/Output/PythonGeneratedOutput/AnisotropyTextFiles/ . The density plot in part b is obtained using (1)/script/GibsonEtAl/Graphing/DensityPlot_Severing.py. For this script, the file names of the output text files need to be manually written into the function Inputs() on line 14 of DensityPlot.py. On the command line this script outputs "rc", "rn" and "anisotropy" averages for each setup. To plot part a of the figure, thoses lists need to be copied into the Matlab script "Simulation Results/Fig8.m" on lines 4-13. Running this script will generate Fig 8a. 

Fig. A1 Data can be obtained by running the tubulaton scripts (1)/script/GibsonEtAl/FigA1\_NoZip and (1)/script/GibsonEtAl/FigA1\_Zip or by using data generated from the previous figures. These two tubulaton scripts will automatically generate some of the output but other plots needs to be generated using seperate scripts. Fig A1c is obtained using the script (1)/script/GibsonEtAl/Graphing/TimeVariation_MultiLine.py . Output from Tubulaton needs to be read into this script by manually writing the file names into a list for the variables FileAll at the start of the script. Fig A1b can either be obtained from the FigA1\_NoZip  and FigA1\_Zip scripts directly or alternatively once the tubulaton output is generated (from these scripts or small variations) the plot can be generated by calling (1)/script/GibsonEtAl/Graphing/SamplePlots from the command line with:
"python3 SamplePlots ../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/Appen_Zip_Den_2023042213291682166570.txt 10 test Appen_NoZip"
where the first input is the text file containing the tubulaton output data, second the number of repeats and the fourth entry the name of the plot to be saved. (Fig A1b was generated this second way). Fig A1d is obtained from running (1)/script/GibsonEtAl/Graphing/DensityPlot_Geff.py For this script, the file names of the tubulaton output text files needs to be manually written into the function Inputs() at the start of the script (see details for plotting Fig 7). 

## Contact

Cameron Gibson, camerongibson@tamu.edu<br>
Henrik Jönsson, henrik.jonsson@slcu.cam.ac.uk<br>
Tamsin Spelman, tamsin.spelman@slcu.cam.ac.uk




